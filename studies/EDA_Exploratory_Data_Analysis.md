# The Basic Process

The idea behind explorator data analysis is to gain insights into the data
that you didn't have before. This process is not set in stone and requires a
good bit of creativity. The general systematic approach goes as follows:

1. Generate questions about you data
2. Search for answers by visualising, transforming, and modelling your data
3. Use what you learn to refine your questions and/to generate new questions.

> "There are no routine statistical questions, only questionable statistical
> routines." -- Sire David Cox

> "Far better an approximate answer to the right question, which is often
> vague, than an exact answer to the wrong question, which can always be
> made precise." -- John Tukey

Use questions as tools. Here are two types of general purpose questions that
can be applied in a lot of cases.

1. What type of variation occurs within my variables?
2. What type of covariation occurs between my variables?

### An example of tabular data


|variable1|variable2|
|---------|---------|
|value1   |value1   | observation1
|value2   |value2   | observation2
|value3   |value3   | observation3

* The data is _tidy_ if there is only one value per "cell"

# Variation

variation is the tendancy of the **values** of a **variable** to change from
measurement to measurement. Every **variable** has its own pattern of variation
which can reveal interesting information. The best way to understand this is to
visualise the distribution of the variables values.

## Categorical varibles

* Usually have a small set of possible values.
* Bar chart is good for viewing distribution. 

## Continuous varibles

* Infinite set of ordered values.
* Histogram/Frequency Poly are good for these variables.

# Useful Questions

* Which values are the most common? Why?
* Which values are rare? Why? Does that match your expectations?
* Can you see any unusual patterns? What might explain them?

### Subgroup questions

* How are the observations within each cluster similar to each other?
* How are the observations in separate clusters different form each other?
* How can you explain or describe the clusters?
* Why might the appearance of clusters be misleading?

# Import Questions

* How are variables related?
